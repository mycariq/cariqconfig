#!/bin/sh

basePath="/var/log/forever"
dateStr=`date +"%Y%m%d"`

mkdir -p ${basePath}
   
#get parsers and receivers and start by forever 
for i in `ls */server.js`; do
    logPath=${basePath}"/"`echo ${i} | cut -d'/' -f 1`
    mkdir -p "${logPath}"
    filePath=${logPath}"/"`echo $i | cut -d'/' -f 2`"-"${dateStr}
    outFile="${filePath}.out"
    logFile="${filePath}.log"
    errFile="${filePath}.err"
    uid=`echo $i | tr / _`
    forever -a -l "${logFile}" -o "${outFile}" -e "${errFile}" --uid "${uid}" start "${i}";
done;
